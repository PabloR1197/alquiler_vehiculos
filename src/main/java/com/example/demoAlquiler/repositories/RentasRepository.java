package com.example.demoAlquiler.repositories;

import com.example.demoAlquiler.models.Rentas;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public interface RentasRepository extends JpaRepository <Rentas, Integer> {


    List<Rentas> findByMediosDePagoDetalles_IdUsuario(Integer idUsuario);

    List<Rentas> findByMediosDePagoDetalles_IdTipoMedioDePago(Integer idTipoMedioDePago);


    List<Rentas> findAllByIdTipoEstadoAlquiler(Integer idTipoEstadoAlquiler);

    List<Rentas> findAllByFechaInicioBetween(Date FechaInicio, Date FechaFin);




}

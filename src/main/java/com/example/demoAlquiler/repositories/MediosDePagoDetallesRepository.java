package com.example.demoAlquiler.repositories;

import com.example.demoAlquiler.models.MediosDePagoDetalles;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MediosDePagoDetallesRepository extends JpaRepository<MediosDePagoDetalles, Integer> {

}

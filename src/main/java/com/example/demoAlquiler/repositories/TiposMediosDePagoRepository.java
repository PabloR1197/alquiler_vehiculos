package com.example.demoAlquiler.repositories;

import com.example.demoAlquiler.models.TiposMediosDePago;
import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TiposMediosDePagoRepository extends JpaRepository<TiposMediosDePago, Integer> {




}




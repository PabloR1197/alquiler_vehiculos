package com.example.demoAlquiler.repositories;

import com.example.demoAlquiler.models.TiposEstadosAlquileres;
import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TiposEstadosAlquileresRepository extends JpaRepository<TiposEstadosAlquileres, Integer> {


}

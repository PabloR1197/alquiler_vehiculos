package com.example.demoAlquiler.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

@Entity
@Table(name = "mediosdepagodetalles")
public class MediosDePagoDetalles {


    private Integer idMedioDePagoDetalle;
    private Integer idUsuario;
    private Integer idTipoMedioDePago;
    private String numeroTarjeta;
    private Usuarios usuarios;
    private TiposMediosDePago tiposMediosDePago;
    private String nombreUsuario;
    private String nombrePila;
    private String apellido;
    private String nombreMedioDePago;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_medio_de_pago_detalle", insertable = false, unique = true)
    public Integer getIdMedioDePagoDetalle() {
        return idMedioDePagoDetalle;
    }

    public void setIdMedioDePagoDetalle(Integer idMedioDePagoDetalle) {
        this.idMedioDePagoDetalle = idMedioDePagoDetalle;
    }

    @Basic
    @Column(name = "id_usuario", nullable = false)
    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Basic
    @Column(name = "id_tipo_medio_de_pago", nullable = false)
    public Integer getIdTipoMedioDePago() {
        return idTipoMedioDePago;
    }

    public void setIdTipoMedioDePago(Integer idTipoMedioDePago) {
        this.idTipoMedioDePago = idTipoMedioDePago;
    }
    @Basic
    @Column(name = "numero_tarjeta")
    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario", insertable = false, updatable = false)
    public Usuarios getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }

    @Transient
    public String getNombreUsuario() {
        return usuarios != null ? usuarios.getNombreUsuario() : null;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    @Transient
    public String getNombrePila() {
        return usuarios != null ? usuarios.getNombrePila() : null;
    }

    public void setNombrePila(String nombrePila) {
        this.nombrePila = nombrePila;
    }

    @Transient
    public String getApellido() {
        return usuarios != null ? usuarios.getApellido() : null;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "id_tipo_medio_de_pago", referencedColumnName = "id_tipo_medio_de_pago", insertable = false, updatable = false)
    public TiposMediosDePago getTiposMediosDePago() {
        return tiposMediosDePago;
    }

    public void setTiposMediosDePago(TiposMediosDePago tiposMediosDePago) {
        this.tiposMediosDePago = tiposMediosDePago;
    }

    @Transient
    public String getNombreMedioDePago() {
        return tiposMediosDePago != null ? tiposMediosDePago.getNombreMedioDePago() : null;
    }

    public void setNombreMedioDePago(String nombreMedioDePago) {
        this.nombreMedioDePago = nombreMedioDePago;
    }



}

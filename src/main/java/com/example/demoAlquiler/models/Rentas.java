package com.example.demoAlquiler.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.sql.Timestamp;

@Entity
@Table(name = "rentas")
public class Rentas {


    private Integer idRenta;
    private String vehiculoRentado;
    private Float valorAlquiler;
    private Integer idMedioDePagoDetalle;
    private Timestamp fechaInicio;
    private Timestamp fechaFin;
    private Integer IdTipoEstadoAlquiler;
    private TiposEstadosAlquileres tiposEstadosAlquileres;
    private MediosDePagoDetalles mediosDePagoDetalles;
    private String nombrePila;
    private String apellido;
    private String nombreMedioDePago;
    private String nombreEstadoAlquiler;





    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_renta", insertable = false, unique = true)
    public Integer getIdRenta() {
        return idRenta;
    }

    public void setIdRenta(Integer idRenta) {
        this.idRenta = idRenta;
    }

    @Basic
    @Column(name = "vehiculo_rentado", nullable = false)
    public String getVehiculoRentado() {
        return vehiculoRentado;
    }

    public void setVehiculoRentado(String vehiculoRentado) {
        this.vehiculoRentado = vehiculoRentado;
    }

    @Basic
    @Column(name = "valor_alquiler", nullable = false)
    public Float getValorAlquiler() {
        return valorAlquiler;
    }

    public void setValorAlquiler(Float valorAlquiler) {
        this.valorAlquiler = valorAlquiler;
    }

    @Basic
    @Column(name = "id_medio_de_pago_detalle", nullable = false)
    public Integer getIdMedioDePagoDetalle() {
        return idMedioDePagoDetalle;
    }

    public void setIdMedioDePagoDetalle(Integer idMedioDePagoDetalle) {
        this.idMedioDePagoDetalle = idMedioDePagoDetalle;
    }

    @Basic
    @Column(name = "fecha_inicio", nullable = false)
    public Timestamp getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Timestamp fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Basic
    @Column(name = "fecha_fin")
    public Timestamp getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Timestamp fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Basic
    @Column(name = "id_tipo_estado_alquiler", nullable = false)
    public Integer getIdTipoEstadoAlquiler() {
        return IdTipoEstadoAlquiler;
    }

    public void setIdTipoEstadoAlquiler(Integer idTipoEstadoAlquiler) {
        this.IdTipoEstadoAlquiler = idTipoEstadoAlquiler;
    }

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "id_medio_de_pago_detalle", referencedColumnName = "id_medio_de_pago_detalle", insertable = false, updatable = false)
    public MediosDePagoDetalles getMediosDePagoDetalles() {
        return mediosDePagoDetalles;
    }

    public void setMediosDePagoDetalles(MediosDePagoDetalles mediosDePagoDetalles) {
        this.mediosDePagoDetalles = mediosDePagoDetalles;
    }

    @Transient
    public String getNombrePila() {
        return mediosDePagoDetalles != null ? mediosDePagoDetalles.getNombrePila() : null;
    }

    public void setNombrePila(String nombrePila) {
        this.nombrePila = nombrePila;
    }

    @Transient
    public String getApellido() {
        return mediosDePagoDetalles != null ? mediosDePagoDetalles.getApellido() : null;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Transient
    public String getNombreMedioDePago() {
        return mediosDePagoDetalles != null ? mediosDePagoDetalles.getNombreMedioDePago() : null;
    }

    public void setNombreMedioDePago(String nombreMedioDePago) {
        this.nombreMedioDePago = nombreMedioDePago;
    }

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "id_tipo_estado_alquiler", referencedColumnName = "id_tipo_estado_alquiler", insertable = false, updatable = false)
    public TiposEstadosAlquileres getTiposEstadosAlquileres() {
        return tiposEstadosAlquileres;
    }

    public void setTiposEstadosAlquileres(TiposEstadosAlquileres tiposEstadosAlquileres) {
        this.tiposEstadosAlquileres = tiposEstadosAlquileres;
    }

    @Transient
    public String getNombreEstadoAlquiler() {
        return tiposEstadosAlquileres != null ? tiposEstadosAlquileres.getNombreEstadoAlquier() : null;
    }

    public void setNombreEstadoAlquiler(String nombreEstadoAlquiler) {
        this.nombreEstadoAlquiler = nombreEstadoAlquiler;
    }






}

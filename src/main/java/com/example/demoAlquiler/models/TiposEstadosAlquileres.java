package com.example.demoAlquiler.models;


import jakarta.persistence.*;

@Entity
@Table(name = "tiposestadosalquileres")
public class TiposEstadosAlquileres {

    private Integer idTipoEstadoAlquiler;
    private String nombreEstadoAlquier;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tipo_estado_alquiler", nullable = false, unique = true)
    public Integer getIdTipoEstadoAlquiler() {
        return idTipoEstadoAlquiler;
    }

    public void setIdTipoEstadoAlquiler(Integer idTipoEstadoAlquiler) {
        this.idTipoEstadoAlquiler = idTipoEstadoAlquiler;
    }

    @Basic
    @Column(name = "nombre_estado_alquiler", nullable = false)
    public String getNombreEstadoAlquier() {
        return nombreEstadoAlquier;
    }

    public void setNombreEstadoAlquier(String nombreEstadoAlquier) {
        this.nombreEstadoAlquier = nombreEstadoAlquier;
    }
}

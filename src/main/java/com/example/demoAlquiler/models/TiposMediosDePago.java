package com.example.demoAlquiler.models;


import jakarta.persistence.*;

@Entity
@Table(name = "tiposmediosdepago")
public class TiposMediosDePago {


    private Integer idTipoMedioDePago;
    private String nombreMedioDePago;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tipo_medio_de_pago", insertable = false, unique = true)
    public Integer getIdTipoMedioDePago() {
        return idTipoMedioDePago;
    }

    public void setIdTipoMedioDePago(Integer idTipoMedioDePago) {
        this.idTipoMedioDePago = idTipoMedioDePago;
    }
    @Basic
    @Column(name = "nombre_medio_de_pago", nullable = false)
    public String getNombreMedioDePago() {
        return nombreMedioDePago;
    }

    public void setNombreMedioDePago(String nombreMedioDePago) {
        this.nombreMedioDePago = nombreMedioDePago;
    }
}





package com.example.demoAlquiler.configuration;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class JpaConfig {

    @Bean
    public DataSource dataSource(){
        return DataSourceBuilder
                .create()
                .url("jdbc:mysql://localhost:3306/empresa_alquiler")
                .username("root")
                .password("Pablo9711")
                .driverClassName("com.mysql.cj.jdbc.Driver")
                .build();
    }



}

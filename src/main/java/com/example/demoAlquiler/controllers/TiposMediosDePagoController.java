package com.example.demoAlquiler.controllers;



import com.example.demoAlquiler.models.TiposMediosDePago;
import com.example.demoAlquiler.repositories.TiposMediosDePagoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/tiposMediosDePago")
public class TiposMediosDePagoController {

    @Autowired
    private TiposMediosDePagoRepository tiposMediosDePagoRepository;

    @RequestMapping(method = RequestMethod.GET)
    List<TiposMediosDePago> findAll() {
        return tiposMediosDePagoRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    TiposMediosDePago buscarTipoPorId(@PathVariable Integer id) {
        TiposMediosDePago tiposMediosDePago = tiposMediosDePagoRepository.findById(id.intValue()).orElse(null);

        return tiposMediosDePago;
    }

}

package com.example.demoAlquiler.controllers;


import com.example.demoAlquiler.models.Rentas;
import com.example.demoAlquiler.repositories.RentasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@RestController
@RequestMapping("/api/rentas")
public class RentasController {

    @Autowired
    private RentasRepository rentasRepository;

    @RequestMapping(method = RequestMethod.GET)
    List<Rentas> findAll() {
        return rentasRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    Rentas buscarTipoPorId(@PathVariable Integer id) {
        Rentas rentas = rentasRepository.findById(id.intValue()).orElse(null);
        return rentas;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/usuario/{idUsuario}")
    List<Rentas>ObtenerListaDeRentasPorIdUsuario(@PathVariable Integer idUsuario) {
        return rentasRepository.findByMediosDePagoDetalles_IdUsuario(idUsuario);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/medioDePago/{idTipoMedioDePago}")
    List<Rentas>ObtenerListaDeRentasPorIdMedioDePago(@PathVariable Integer idTipoMedioDePago) {
        return rentasRepository.findByMediosDePagoDetalles_IdTipoMedioDePago(idTipoMedioDePago);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/estadoAlquiler/{idTipoEstadoAlquiler}")
    List<Rentas>ObtenerListaDeRentasPorIdEstadoAlquiler(@PathVariable Integer idTipoEstadoAlquiler) {
        return rentasRepository.findAllByIdTipoEstadoAlquiler(idTipoEstadoAlquiler);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/filtroFechas/{FechaInicio}/{FechaFin}")
    List<Rentas> findByFechaInicionBetween(@PathVariable String FechaInicio, @PathVariable String FechaFin) throws ParseException {
        SimpleDateFormat fInicio = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat fFin = new SimpleDateFormat("yyyy-MM-dd");
        return rentasRepository.findAllByFechaInicioBetween(fInicio.parse(FechaInicio), fFin.parse(FechaFin));
    }


    @PostMapping
    public ResponseEntity<Rentas> agregarRenta(@RequestBody Rentas rentas) {

        Rentas nuevaRenta = rentasRepository.save(rentas);
        return ResponseEntity.status(HttpStatus.CREATED).body(nuevaRenta);
    }

    @PutMapping("{id}")
    public Rentas updateRentaEstado(@PathVariable(value = "id") Integer idRenta, @RequestBody Rentas renta) {
        Rentas rentaToUpdate = rentasRepository.findById(idRenta)
                .orElseThrow(() -> new ResourceNotFoundException("Renta", "id", idRenta));
        rentaToUpdate.setIdTipoEstadoAlquiler(renta.getIdTipoEstadoAlquiler());
        if(renta.getIdTipoEstadoAlquiler() == 2) {
            rentaToUpdate.setFechaFin(new Timestamp(System.currentTimeMillis()));
        }

        Rentas updatedRenta = rentasRepository.save(rentaToUpdate);
        return updatedRenta;
    }



}

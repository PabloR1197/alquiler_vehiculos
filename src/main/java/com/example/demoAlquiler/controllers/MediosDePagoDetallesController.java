package com.example.demoAlquiler.controllers;


import com.example.demoAlquiler.models.MediosDePagoDetalles;
import com.example.demoAlquiler.models.Usuarios;
import com.example.demoAlquiler.repositories.MediosDePagoDetallesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/mediosDePagoDetalles")
public class MediosDePagoDetallesController {

    @Autowired
    private MediosDePagoDetallesRepository mediosDePagoDetallesRepository;

    @RequestMapping(method = RequestMethod.GET)
    List<MediosDePagoDetalles> findAll() {
        return mediosDePagoDetallesRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    MediosDePagoDetalles buscarPorId(@PathVariable Integer id) {
        MediosDePagoDetalles mediosDePagoDetalles = mediosDePagoDetallesRepository.findById(id.intValue()).orElse(null);
        return mediosDePagoDetalles;
    }

    @PostMapping
    public ResponseEntity<MediosDePagoDetalles> AgregarMedioDePagoUsuario(@RequestBody MediosDePagoDetalles mediosDePagoDetalles) {
        MediosDePagoDetalles nuevoMedioDePago = mediosDePagoDetallesRepository.save(mediosDePagoDetalles);
        return ResponseEntity.status(HttpStatus.CREATED).body(nuevoMedioDePago);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminarMedioDePago(@PathVariable Integer id) {
        mediosDePagoDetallesRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<MediosDePagoDetalles> actualizarMedioDePagoDetalle(@PathVariable(value = "id") Integer idMedioDePagoDetalle, @RequestBody MediosDePagoDetalles medioDePagoDetalleModificado) throws ResourceNotFoundException {
        MediosDePagoDetalles medioDePagoDetalle = mediosDePagoDetallesRepository.findById(idMedioDePagoDetalle)
                .orElseThrow(() -> new ResourceNotFoundException("No existe un Medio De Pago Asociafo Al Id" + idMedioDePagoDetalle));

        medioDePagoDetalle.setIdUsuario(medioDePagoDetalleModificado.getIdUsuario());
        medioDePagoDetalle.setIdTipoMedioDePago(medioDePagoDetalleModificado.getIdTipoMedioDePago());
        medioDePagoDetalle.setNumeroTarjeta(medioDePagoDetalleModificado.getNumeroTarjeta());


        final MediosDePagoDetalles medioDePagoDetalleActualizado = mediosDePagoDetallesRepository.save(medioDePagoDetalle);
        return ResponseEntity.ok(medioDePagoDetalleActualizado);
    }








}

package com.example.demoAlquiler.controllers;


import com.example.demoAlquiler.models.TiposEstadosAlquileres;
import com.example.demoAlquiler.repositories.TiposEstadosAlquileresRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/tiposEstadosAlquileres")
public class TiposEstadosAlquileresController {


    @Autowired
    private TiposEstadosAlquileresRepository tiposEstadosAlquileresRepository;


    @RequestMapping(method = RequestMethod.GET)
    List<TiposEstadosAlquileres> findAll() {
        return tiposEstadosAlquileresRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    TiposEstadosAlquileres buscarTipoPorId(@PathVariable Integer id) {
        TiposEstadosAlquileres tiposEstadosAlquileres = tiposEstadosAlquileresRepository.findById(id.intValue()).orElse(null);
        return tiposEstadosAlquileres;
    }



}

package com.example.demoAlquiler.controllers;



import com.example.demoAlquiler.models.Usuarios;
import com.example.demoAlquiler.repositories.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/usuarios")
public class UsuariosController {

    @Autowired
    private UsuariosRepository usuariosRepository;

    @RequestMapping(method = RequestMethod.GET)
    List<Usuarios> findAll() {
        return usuariosRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    Usuarios buscarPorId(@PathVariable Integer id) {
        Usuarios usuarios = usuariosRepository.findById(id.intValue()).orElse(null);
        return usuarios;
    }

    @PostMapping
    public ResponseEntity<Usuarios> AgregarUsuario(@RequestBody Usuarios usuario) {
        Usuarios nuevoUsuario = usuariosRepository.save(usuario);
        return ResponseEntity.status(HttpStatus.CREATED).body(nuevoUsuario);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminarUsuario(@PathVariable Integer id) {
        usuariosRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }










}

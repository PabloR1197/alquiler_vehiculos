package com.example.demoAlquiler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoAlquilerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoAlquilerApplication.class, args);
	}

}

Este proyecto fue creado para cumplir los requerimientos relacionados con respecto a la empresa de alquiler de vehículos:

Se contruyeron las siguientes clases/tablas para el manejo de la información:

1. Usuarios

La tabla en dónde estarán registrados aquellos usuarios que realicen algún servicio de alquiler de autor

2. Tipos Medios De Pago

La tabla en dónde estarán listadas las formas a las que puede recurrir un usuario para poder pagar el alquiler (Efectivo, Tarjeta Débito, Tarjeta Crédito)

3. Tipos Estados Alquileres

La tabla en la cual estarán listados los estados en los que se puede encontrar un alquiler al momento que sea registrado (Abierto, Cerrodo, En Revisión)

4. Medios De Pago Detalles

Aquí se va a relacionar el usuario con el método de pago que usará, todo relacionado en un id_medio_de_pago_detalle, también hay creado un campo adicional para poder registrar el número de la tarjeta débito o crédito que usará

5. Rentas

En este lugar se registrarán los alquileres, con el vehículo, el valor, la fecha de inicio y la fecha final, tendrá un id_medio_de_pago_detalle que mostrará que usuario realizó el alquiler y que medio de pago fue usado para esto mismo. También tendrá un campo en dónde se registrará el id_estado_alquiler que va tener asignado esa renta en específico.


En Usuarios se creearon los microservicios relacionados con la creación, eliminación y consulta de usuarios, teniendo en cuenta que no hay ningún dato especial que corresponda a un grupo de usuarios, la consulta se puede realizar en general (todos los usuarios) o buscar el usuario por su id principal.

En las tablas de tipos estados alquileres y tipos medios de pago únicamente se pueden consultar los registros de la tabla, no hay opción de eliminación, modificación o creación de nuevos registros.

La tabla Medios de pago detalles se puede consultar en general, se pueden agregar más medios de pago a un usuario o modificar los ya existentes. 

Para finalizar, en la tabla Rentas se pueden crear nuevos alquileres, y modificarlos (únicamente es permitido modificar el id_estado_alquiler) también, al momento de modificar el id_estado_alquiler por el estado que corresponde a "cerrado", se hará una incersión de la fecha automática en el campo fecha fin. 

También se pueden consultar las Rentas por distintos criterios(Consulta en la tabal general, consulta por el id_renta, consultar todas las rentas de un solo usuario, consultar todas las rentas que fueron realizadas con un medio de pago específico, consultar todas las rentas con un estado específico y consultar las rentas iniciadas en un rango de fecha específico)




